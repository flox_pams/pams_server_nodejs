const Ping = require('ping-lite');
const axios = require('axios');
const fs = require('fs');
const worker_settings = JSON.parse(fs.readFileSync("settings/worker.json"));
const notification_handler = require('./notifications_handler/' + worker_settings.notification_handler  + '.js');

let old_status = JSON.parse(fs.readFileSync("status.json"));
let services = JSON.parse(fs.readFileSync("settings/services.json")).services;
let result = new Object;


Object.keys(services).forEach(key => {
    let element = services[key];
    if (element.probe == "ping"){
        var ping = new Ping(element.ip);
        ping.send(function(err, ms) {
            if (ms == null){
                send_notification(key, false);
                result[key] = new Object;
                result[key].online = false;
            }else{
                send_notification(key, true);
                result[key] = new Object;
                result[key].latency = ms;
                result[key].online = true;    
            }

        });
    }else if (element.probe == "curl"){
        let msstart = Date.now();
        axios.get(element.address)
            .then(function (response) {
                if (response.status == 200){
                    send_notification(key, true);
                    result[key] = new Object;
                    result[key].latency = Math.round((Date.now() - msstart)/10);
                    result[key].online = true;  
                }else{
                    send_notification(key, false);
                    result[key] = new Object;
                    result[key].online = false;
                }
            })
    }
});

process.on('exit', () => {
    console.log(result);
    fs.writeFileSync("status.json", JSON.stringify(result));
    fs.writeFileSync("lastcheck.txt", JSON.stringify(new Date().getTime()));
});

function send_notification(key, new_status){
    if (typeof old_status[key] == "undefined") return;
    notification_handler.send(old_status[key].online, new_status, services[key]);
}
/*

while(true){
    console.log(result);
}








});

var pinglist = ['8.8.8.8','1.1.1'];
var curllist = ['https://flo-x.fr','https://1.1.1.1']

pinglist.forEach((element) =>{
    var ping = new Ping(element);
    ping.send(function(err, ms) {
        if (ms == null){
            console.log(ping._host+ " - No Response");
        }else{
            console.log(ping._host+' - '+ms+"ms");    
        }

    });
});

curllist.forEach((element) =>{
    axios.get(element)
    .then(function (response) {
        console.log(element + " - " + response.status);
    })
});
*/