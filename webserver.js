var http = require('http');
const fs = require('fs');
var md5 = require('md5');
const { parse, json, text, form, jsonTypes } = require('get-body');
var sha256 = require('js-sha256');
let config = JSON.parse(fs.readFileSync("settings/web.json"));
var server = http.createServer(function (req, res) { 
    let path = req.url.split("/").slice(1);  
    if (req.url == '/') {
        let page = fs.readFileSync('html/assets/static.html').toString();
        page = page.replace("{{page_title}}", config.web_page.page_title);
        page = page.replace("{{navbar_title}}", config.web_page.navbar_title);
        if (check_admin(req)){
            let adminjs = fs.readFileSync('html/admin-js.html').toString().replace("{{admin_user}}", config.admin.username);
            page = page.replace("{{admin_js}}", adminjs);
        }else{
            page = page.replace("{{admin_js}}", "");
        }
        page = page.replace("{{lastcheck}}", timestamptodate(fs.readFileSync("lastcheck.txt")*1));
        if (config.web_page.navbar_logo.show){
            page = page.replace("{{navbar_logo}}", '<div class="flox-bar-item"><img src="' + config.web_page.navbar_logo.url + '" style="margin-top: ' + config.web_page.navbar_logo.offset + 'px;" height=25px  alt="Logo"></div>');
        }else{
            page = page.replace("{{navbar_logo}}", "");
        }
        page = page.replace("{{services_list}}", gen_html(check_admin(req)));
        page = page.replace("/*{{autorefresh_delay}}*/", config.autorefresh.delay);
        res.writeHead(200, { 'Content-Type': 'text/html' });
        res.write(page);
        res.end();
    }else if (path[0] == "static"){
        if (fs.existsSync('html/assets/' + path.slice(1).join("/"))){
            let page = fs.readFileSync('html/assets/' + path.slice(1).join("/"));
            res.writeHead(200, { 'Content-Type': 'text/html' });
            res.write(page);
            res.end();   
        }else{
            res.writeHead(404);
            res.write("404");
            res.end();
        }
    }else if (path[0] == "favicon.ico"){
        if (fs.existsSync('html/assets/favicon.ico')){
            let page = fs.readFileSync('html/assets/favicon.ico');
            res.writeHead(200, { 'Content-Type': 'image/x-icon' });
            res.write(page);
            res.end();   
        }else{
            res.writeHead(404);
            res.write("404");
            res.end();
        }
    }else if (path[0] == "getchecksum"){
        if (config.autorefresh.enabled == true){
            res.writeHead(200, { 'Content-Type': 'text/html' });
            res.write(gen_checksum()); 
        }else{
            res.writeHead(200, { 'Content-Type': 'text/html' });
            res.write("disabled"); 
        }
        res.end();   

    }else if (path[0] == "api"){
        if (typeof path[1] != "undefined"){
            if (sha256(path[1].toString()) == config.api.token){
                res.writeHead(200);
                res.write(JSON.stringify(gen_api()));
            }else{
                res.writeHead(401);
                res.write("invalid_token");
            }        
        }else{
            res.writeHead(401);
            res.write("no_token");
        }
        
        res.end();
    }else if (path[0] == "admin"){
        if (check_admin(req)){
            if (path[1] == "changemsg"){
                let services = JSON.parse(fs.readFileSync("settings/services.json"));
                services.services[path[2]].msg_txt = decodeURI(path[3]);
                fs.writeFileSync("settings/services.json", JSON.stringify(services, null, 5));
                res.writeHead(200);
                res.write('<script>document.location.href="/"</script>');
                res.end();
            }else if(path[1] == "changeshowping"){
                let services = JSON.parse(fs.readFileSync("settings/services.json"));
                if (path[3] == 'true'){
                services.services[path[2]].show_ping = true; 
                }else{
                    services.services[path[2]].show_ping = false; 
                }
                fs.writeFileSync("settings/services.json", JSON.stringify(services, null, 5));
                res.writeHead(200);
                res.write('<script>document.location.href="/"</script>');
                res.end();
            }else if(path[1] == "changemode"){
                let services = JSON.parse(fs.readFileSync("settings/services.json"));
                services.services[path[2]].mode = path[3]; 
                fs.writeFileSync("settings/services.json", JSON.stringify(services, null, 5));
                res.writeHead(200);
                res.write('<script>document.location.href="/"</script>');
                res.end();
            }else if(path[1] == "deleteservice"){
                let services = JSON.parse(fs.readFileSync("settings/services.json"));
                delete services.services[path[2]]; 
                fs.writeFileSync("settings/services.json", JSON.stringify(services, null, 5));
                res.writeHead(200);
                res.write('<script>document.location.href="/"</script>');
                res.end();
            }else if(path[1] == "advanced-settings"){
                let page = fs.readFileSync('html/admin.html').toString();
                page = page.replace("{{services_json}}", fs.readFileSync("settings/services.json"));
                page = page.replace("{{web_json}}", fs.readFileSync("settings/web.json"));
                page = page.replace("{{worker_json}}", fs.readFileSync("settings/worker.json"));
                if(path[2] == "err"){
                    page = page + '<script>$.notify("&nbsp;&nbsp;<text class=\'flox-text-white\'>An error has occurred!</text>", {blur: 0.0, align:"right", verticalAlign:"top", type: "info", icon:"exclamation-triangle", background: "#ff0004", close: false, delay:0});</script>'
                }
                if(path[2] == "success"){
                    page = page + '<script>$.notify("&nbsp;&nbsp;<text class=\'flox-text-white\'>The settings have been saved!</text>", {blur: 0.0, align:"right", verticalAlign:"top", type: "info", icon:"check", background: "#00bd0d", close: false, delay:0});</script>'
                }
                res.writeHead(200);
                res.write(page);
                res.end();
            }else if(path[1] == "advanced-settings-set"){
                if (req.method == "POST"){
                    parse(req, req.headers).then(body => {
                        let can_continue = true;
                        try {
                            fs.writeFileSync("settings/services.json", JSON.stringify(JSON.parse(body.services_settings), null, 5));
                            fs.writeFileSync("settings/worker.json", JSON.stringify(JSON.parse(body.worker_settings), null, 5));
                            fs.writeFileSync("settings/web.json", JSON.stringify(JSON.parse(body.web_settings), null, 5));
                        }catch(err) {
                            can_continue = false
                        }
                        if (can_continue){
                            res.writeHead(200, {'Content-Type': 'text/html'})
                            res.end('<script>document.location.href="/admin/advanced-settings/success"</script>')                       
                        }else{
                            res.writeHead(200, {'Content-Type': 'text/html'})
                            res.end('<script>document.location.href="/admin/advanced-settings/err"</script>')   
                        }

                    });                    
                }else{
                    res.writeHead(405)
                    res.end('Method Not Allowed')   
                }
            }else if(path[1] == "createservice"){
                if (req.method == "POST"){
                    parse(req, req.headers).then(body => {
                        let services = JSON.parse(fs.readFileSync("settings/services.json"));
                        if (isset(body.name) && isset(body.group) && isset(body["fa-icon"]) && isset(body.probe)){
                            if (typeof services.services[body.uuid] == "undefined"){
                                services.services[body.uuid] = new Object;
                                services.services[body.uuid].msg_txt = "";
                                services.services[body.uuid].mode = "auto";
                                services.services[body.uuid].show_ping = true;
                            }
                            services.services[body.uuid].name = body.name;
                            services.services[body.uuid].group = body.group;
                            services.services[body.uuid].fa_icon = body["fa-icon"];
                            if (body.probe == "curl"){
                                services.services[body.uuid].probe = "curl";
                                services.services[body.uuid].address = body.address;
                            }else if(body.probe == "ping"){
                                services.services[body.uuid].probe = "ping";
                                services.services[body.uuid].ip = body.ip;
                            }
                            fs.writeFileSync("settings/services.json", JSON.stringify(services, null, 5));
                            res.writeHead(200, {'Content-Type': 'text/html'});
                            res.write('<script>document.location.href="/"</script>');
                            res.end();
                        }else{
                            res.writeHead(500, {'Content-Type': 'text/html'});
                            res.write('value_missing');
                            res.end();
                        }
                    });                    
                }else{
                    res.writeHead(405)
                    res.end('Method Not Allowed')   
                }
            }else if(path[1] == "setnewapitoken"){
                if (req.method == "POST"){
                    parse(req, req.headers).then(body => {
                        if (isset(body.token)){
                            let settings_w = JSON.parse(fs.readFileSync("settings/web.json"));
                            let sha = sha256(body.token);
                            settings_w.api.token = sha;
                            config.api.token = sha;
                            fs.writeFileSync("settings/web.json", JSON.stringify(settings_w, null, 5));
                            res.writeHead(200, {'Content-Type': 'text/html'});
                            res.write('<script>document.location.href="/"</script>');
                            res.end();
                        }else{
                            res.writeHead(500, {'Content-Type': 'text/html'});
                            res.write('value_missing');
                            res.end();
                        }
                    });                    
                }else{
                    res.writeHead(405)
                    res.end('Method Not Allowed')   
                }
            }else if(path[1] == "setnewadmin"){
                if (req.method == "POST"){
                    parse(req, req.headers).then(body => {
                        let settings_w = JSON.parse(fs.readFileSync("settings/web.json"));
                        let sha = sha256(body.password);
                        settings_w.admin.username = body.username
                        settings_w.admin.password = sha;
                        config.admin.username = body.username;
                        config.admin.password = sha;
                        fs.writeFileSync("settings/web.json", JSON.stringify(settings_w, null, 5));
                        res.writeHead(200, {'Content-Type': 'text/html'});
                        res.write('<script>document.location.href="/admin"</script>');
                        res.end();

                    });                    
                }else{
                    res.writeHead(405)
                    res.end('Method Not Allowed')   
                }
            }else{
                res.writeHead(200, { 'Content-Type': 'text/html' });
                res.write('<script>document.location.href="/"</script>');
                res.end();
            }
        }else{
            res.setHeader('WWW-Authenticate', 'Basic');
            res.writeHead(401);
            res.write('Not authorized');
            res.end();
        }
    
    }else{
        res.writeHead(404);
        res.write("404");
        res.end();
    }
});

server.listen(config.webserver_port);

console.log('Webserver is listening...')

function gen_checksum(){
    let services = JSON.parse(fs.readFileSync("settings/services.json")).services;
    let status = JSON.parse(fs.readFileSync("status.json"));
    let response = new Object;
    let can_respond = true;
    Object.keys(services).forEach(key => {
        let element = services[key];
        if (typeof status[key] == "undefined"){
            can_respond = false;
        }else{
            let elm_status = status[key];
            if (typeof response[element.group] == "undefined") response[element.group] = new Array;
            let crt_service = new Object;
            crt_service.uuid = key;
            crt_service.online = elm_status.online;
            crt_service.mode = element.mode;
            crt_service.msg_txt = element.msg_txt;
            response[element.group].push(crt_service);
        }

    });
    if (can_respond) return md5(JSON.stringify(response));
    return "false";
}

function gen_api(aff_id = false){
    let services = JSON.parse(fs.readFileSync("settings/services.json")).services;
    let status = JSON.parse(fs.readFileSync("status.json"));
    let response = new Object;
    let can_respond = true;
    Object.keys(services).forEach(key => {
        let element = services[key];
        if (typeof status[key] == "undefined"){
            let elm_status = status[key];
            if (typeof response[element.group] == "undefined") response[element.group] = new Array;
            let crt_service = new Object;
            crt_service.name = element.name;
            crt_service.online = false;
            crt_service.mode = element.mode;
            crt_service.show_ping = element.show_ping;
            crt_service.ping = 0;
            crt_service.msg_txt = element.msg_txt;
            crt_service.fa_icon = element.fa_icon;
            if (aff_id){
                crt_service.uuid = key;
                crt_service.ip = element.ip;
                crt_service.address = element.address;
                crt_service.probe = element.probe;
            }
            response[element.group].push(crt_service);
        }else{
            let elm_status = status[key];
            if (typeof response[element.group] == "undefined") response[element.group] = new Array;
            let crt_service = new Object;
            crt_service.name = element.name;
            crt_service.online = elm_status.online;
            crt_service.mode = element.mode;
            crt_service.show_ping = element.show_ping;
            crt_service.ping = elm_status.latency;
            crt_service.msg_txt = element.msg_txt;
            crt_service.fa_icon = element.fa_icon;
            if (aff_id){
                crt_service.uuid = key;
                crt_service.ip = element.ip;
                crt_service.address = element.address;
                crt_service.probe = element.probe;
            }
            response[element.group].push(crt_service);
        }

    });
    if (can_respond) return response;
    return false;
}
function gen_html(admin = false){
    let response = "";
    const group_title_base = fs.readFileSync("html/group-title.html").toString();
    const service_base = fs.readFileSync("html/service-base.html").toString();
    const services = gen_api(true);
    if (services === false){
        return "<script>alert(\"A manual modification of the configuration file has been detected, wait for an update cycle or restart the worker.\");</script>";
    }
    Object.keys(services).forEach(key => {
        let element = services[key];
        response = response + group_title_base.replace("{{group_name}}", key);
        element.forEach(element => {
            let right_txt
            let color
            let fa_icon
            let can_show_ping = true;
            if (element.mode == "auto"){
                if (element.online){
                    color = "flox-green";
                    fa_icon = "check";
                }else{
                    color = "flox-red";
                    fa_icon = "times";
                    can_show_ping = false;
                }
            }else if (element.mode == "warn"){
                color = "flox-orange";
                fa_icon = "exclamation";
            }else if (element.mode == "force_online"){
                color = "flox-green";
                fa_icon = "check";
            }else if (element.mode == "force_offline"){
                color = "flox-red";
                fa_icon = "times";
                can_show_ping = false;
            }else if (element.mode == "maintenance"){
                color = "flox-blue";
                fa_icon = "wrench";
            }
            if (can_show_ping && element.show_ping){
                right_txt = "<i class='fa fa-" + fa_icon + "'></i> - " + element.ping + "ms&nbsp;";
            }else{
                right_txt ="<i class='fa fa-" + fa_icon + "'></i>&nbsp;&nbsp;";
            }
            msg_txt = element.msg_txt
            
            if (admin){
                msg_txt = msg_txt + "<br>[<a href='#' onclick='changeMsg(\"" + element.uuid + "\")'>Change Message</a>"
                if (element.show_ping){
                    msg_txt = msg_txt + " - <a href='#' onclick='changeShowPing(\"" + element.uuid + "\", true)'>Hide Latency</a>"
                }else{
                    msg_txt = msg_txt + " - <a href='#' onclick='changeShowPing(\"" + element.uuid + "\", false)'>Show Latency</a>"
                }
                msg_txt = msg_txt + " - <a href='#' onclick='changeMode(\"" + element.uuid + "\", \"" + element.mode  + "\")'>Change Mode</a> - <a href='#' onclick='deleteService(\"" + element.uuid + "\")'>Delete</a> - <a href='#' onclick='openChangeServiceModal(" + `"${element.uuid}","${element.name}","${key}","${element.probe}","${element.ip}","${element.address}","${element.fa_icon}"` + ")'>Config</a>]"
            }
            
            response = response + service_base.replace("{{item_name}}", element.name).replace("{{item_txt}}", msg_txt).replace("{{item_color}}", color).replace("{{item_txt_right}}", right_txt).replace("{{fa_icon}}", element.fa_icon);
        });
    });
    return response;
}

function check_admin(req){
    var header = req.headers.authorization || '';
    var token = header.split(/\s+/).pop() || '';
    var auth = Buffer.from(token, 'base64').toString();
    var parts = auth.split(/:/);
    var username = parts.shift();
    var password = parts.join(':');
    if (username == config.admin.username && sha256(password) == config.admin.password){
        return true;
    }
    return false;
}

function timestamptodate(timestamp){
    let date = new Date(timestamp);
    let date_txt =  date.toLocaleDateString("en-US", { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' });
    let hour = date.getHours()
    let minutes = date.getMinutes()
    if (hour < 10){
        hour = "0" + hour.toString();
    }
    if (minutes < 10){
        minutes = "0" + minutes.toString();
    }
    return date_txt + " - " + hour + ":" + minutes;
}
function isset(value){
    if (typeof value == "undefined") return false;
    if (typeof value == 'null') return false;
    if (value == null) return false;
    return true;
}