const {Webhook} = require('simple-discord-webhooks');
module.export = function send(old_status, new_status, service){
    if (new_status == false && new_status != old_status){
        const webhook = new Webhook('https://discord.com/api/webhooks/xxxxxxxxxxxxxxxxxxxxxxxxxxx');
        let embed = {
            title: ":red_square: Unreacheable service",
            type: "rich",
            description: "** **\n:globe_with_meridians: Impacted Service: **" + service.name +  "**\n ** ** \n** **",
            footer: {
                "text": "Auto-Generated by FloX PaMS",
            }    
        }

        webhook.send('', embed); 
    }
}