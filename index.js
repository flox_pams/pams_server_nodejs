const { fork } = require('child_process');
const fs = require('fs');

console.log("Running FloX PaMS Server - NodeJS Edition");
console.log("Starting pre-start test...");
if (!fs.existsSync("settings/services.json")){
    console.log("TEST FAILED: Services Settings file doesn't exist !");
    process.exit();
}
if (!fs.existsSync("settings/web.json")){
    console.log("TEST FAILED: Web Settings file doesn't exist !");
    process.exit();
}
if (!fs.existsSync("settings/worker.json")){
    console.log("TEST FAILED: Worker Settings file doesn't exist !");
    process.exit();
}
try{
    JSON.parse(fs.readFileSync("settings/services.json"));
} catch (error) {
    console.log("TEST FAILED: Services Settings file seems corrupted !");
    process.exit();
}
try{
    JSON.parse(fs.readFileSync("settings/web.json"));
} catch (error) {
    console.log("TEST FAILED: Web Settings file seems corrupted !");
    process.exit();
}
let worker_settings
try{
    worker_settings = JSON.parse(fs.readFileSync("settings/worker.json"));
} catch (error) {
    console.log("TEST FAILED: Worker Settings file seems corrupted !");
    process.exit();
}

console.log("Test OK - Starting the Worker and the Webserver....")

fork("webserver.js");
fork("worker.js");
setInterval(() => {
    fork("worker.js");
}, worker_settings.autorefresh_frequency * 1000)

